package pages;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Locale;

import static org.junit.Assert.assertEquals;

public class SignInPage {

    private final WebDriver driver;

    @FindBy(id = "email_create")
    WebElement txtEmailCreate;

    @FindBy(id = "SubmitCreate")
    WebElement btnSubmitCreate;

    @FindBy(id = "email")
    WebElement txtEmail;

    @FindBy(id = "passwd")
    WebElement txtPassword;

    @FindBy(id = "SubmitLogin")
    WebElement btnSignIn;

    @FindBy(xpath = "(//*[@class='alert alert-danger'])[1]")
    WebElement txtError;

    public SignInPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterCreationEmail() {
        FakeValuesService fakeValuesService = new FakeValuesService(new Locale("de-DE"), new RandomService());
        txtEmailCreate.sendKeys(fakeValuesService.bothify("????##@sample.com"));
    }

    public void clickCreateButton() {
        btnSubmitCreate.click();
    }

    public void enterEmailAndPassword(String username, String password) {
        txtEmail.sendKeys(username);
        txtPassword.sendKeys(password);
    }

    public void clickSignInButton() {
        btnSignIn.click();
    }

    public void verifyErrorMessage() {
        assertEquals("Should display correct error message", "There is 1 error\n" +
                "Authentication failed.", txtError.getText());
    }
}
