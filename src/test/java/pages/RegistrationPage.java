package pages;

import com.github.javafaker.Faker;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RegistrationPage {

    private final WebDriver driver;

    @FindBy(id = "account-creation_form")
    WebElement accountCreationForm;

    @FindBy(id = "id_gender1")
    WebElement rdoMale;

    @FindBy(id = "id_gender2")
    WebElement rdoFemale;

    @FindBy(id = "customer_firstname")
    WebElement txtFirstName;

    @FindBy(id = "customer_lastname")
    WebElement txtLastName;

    @FindBy(id = "passwd")
    WebElement txtPassword;

    @FindBy(xpath = "//*[@id='days']/*[@value='1']")
    WebElement birthDateDay;

    @FindBy(xpath = "//*[@id='months']/*[@value='1']")
    WebElement birthDateMonth;

    @FindBy(xpath = "//*[@id='years']/*[@value='2000']")
    WebElement birthDateYear;

    @FindBy(id = "firstname")
    WebElement txtFirstNameAddress;

    @FindBy(id = "lastname")
    WebElement txtLastNameAddress;

    @FindBy(id = "company")
    WebElement txtCompany;

    @FindBy(id = "address1")
    WebElement txtAddress1;

    @FindBy(id = "address2")
    WebElement txtAddress2;

    @FindBy(id = "city")
    WebElement txtCity;

    @FindBy(xpath = "//*[@id='id_state']/*[@value='1']")
    WebElement ddlState;

    @FindBy(id = "postcode")
    WebElement txtPostalCode;

    @FindBy(xpath = "//*[@id='id_country']/*[@value='21']")
    WebElement ddlCountry;

    @FindBy(id = "phone_mobile")
    WebElement txtMobilePhone;

    @FindBy(id = "submitAccount")
    WebElement btnRegister;

    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void verifyAccountCreationFormDisplayed() {
        assertTrue("Account creation form should be displayed", accountCreationForm.isDisplayed());
    }

    public void enterPersonalInformation(Gender gender) {
        switch (gender) {
            case MALE:
                rdoMale.click();
                break;

            case FEMALE:
                rdoFemale.click();
                break;

            default:
                break;
        }

        Faker faker = new Faker();
        String firstName = faker.address().firstName();
        String lastName = faker.address().lastName();

        txtFirstName.sendKeys(firstName);
        txtLastName.sendKeys(lastName);
        txtPassword.sendKeys(faker.bothify("##??#?###???"));
        birthDateDay.click();
        birthDateMonth.click();
        birthDateYear.click();
        assertEquals("First name for address should match customer's first name", txtFirstNameAddress.getText(), txtFirstName.getText());
        assertEquals("Last name for address should match customer's last name", txtLastNameAddress.getText(), txtLastName.getText());
        txtCompany.sendKeys(faker.company().name());
        txtAddress1.sendKeys(faker.address().fullAddress());
        txtAddress2.sendKeys(faker.address().secondaryAddress());
        ddlCountry.click();
        txtCity.sendKeys(faker.address().city());
        ddlState.click();
        txtPostalCode.sendKeys(faker.numerify("#####"));
        txtMobilePhone.sendKeys(faker.phoneNumber().cellPhone());
    }

    public void clickRegisterButton() {
        btnRegister.click();
    }

    public void verifyRegistrationSuccessful() {
        assertEquals("Browser Title should be 'My account - My Store", "My account - My Store", driver.getTitle());
    }

    public enum Gender {
        MALE,
        FEMALE
    }

}
