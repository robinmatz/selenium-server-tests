package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

public class HomePage {

    private final WebDriver driver;

    @FindBy(xpath = "//a[contains(text(),'Register')]")
    WebElement linkRegister;

    @FindBy(xpath = "//a[contains(text(),'Sign in')]")
    WebElement signIn;

    @FindBy(xpath = "//*[@title='View my customer account']")
    WebElement linkAccount;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openHomePage() {
        String HOME_PAGE_URL = "http://automationpractice.com/index.php";
        driver.get(HOME_PAGE_URL);
    }

    public void goToSignInPage() {
        signIn.click();
    }

    public void goToRegistrationPage() {
        linkRegister.click();
    }

    public void verifyLoggedIn() {
        assertTrue("Account link should be displayed", linkAccount.isDisplayed());
    }
}
