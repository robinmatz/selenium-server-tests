package stepdefinitions;

import common.DriverHelper;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.messages.internal.com.google.common.net.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Hook {

    private final DriverHelper driverHelper;
    private static final Logger LOG = LogManager.getLogger(Hook.class);

    public Hook(DriverHelper driverHelper) {
        this.driverHelper = driverHelper;
    }

    @Before
    public void initDriver() throws MalformedURLException {
        // Default option is to run locally
        boolean isRemote = false;

        // However, this can be overridden
        // by running via VM argument
        // like 'mvn test -Dremote' with
        if (System.getProperty("remote") != null) {
            isRemote = true;
        }

        // Default URL for selenium hub is localhost on port 4444
        String HUB_URL = "http://localhost:4444/wd/hub";

        // However, when launched in Pipeline,
        // it is necessary that the hub url be set
        // via VM arguments like 'mvn clean test -DHUB_URL=<HUB_URL>'
        if (System.getProperty("HUB_URL") != null) {
            HUB_URL = System.getProperty("HUB_URL");
        }

        if (isRemote) {
            LOG.info("Running Selenium Grid Server on HUB_URL: " + HUB_URL);
        } else {
            LOG.info("Running locally");
        }

        // Default browser is chrome
        String browser = "chrome";

        // However, this can be overridden by using
        // VM argument like 'mvn clean test -browser=firefox'
        if (System.getProperty("browser") != null) {
            browser = System.getProperty("browser");
        }

        // Switched through the browser variable
        // According to the value of the browser variable
        // a different RemoteDriver instance will be created and launched
        switch (browser) {
            case "chrome": {
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--launch-maximized");
                options.addArguments("--lang=en-US");

                if (isRemote) {
                    driverHelper.driver = new RemoteWebDriver(new URL(HUB_URL), options);
                } else {
                    System.setProperty("webdriver.chrome.driver", "bin/chromedriver.exe");
                    driverHelper.driver = new ChromeDriver(options);
                }
                driverHelper.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                break;
            }

            case "firefox": {
                FirefoxOptions options = new FirefoxOptions();
                options.addArguments("--launch-maximized");
                options.addArguments("--lang=en-US");

                if (isRemote) {
                    driverHelper.driver = new RemoteWebDriver(new URL(HUB_URL), options);
                } else {
                    System.setProperty("webdriver.firefox.driver", "bin/geckodriver.exe");
                    driverHelper.driver = new FirefoxDriver(options);
                }
                driverHelper.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                break;
            }

            case "opera": {
                OperaOptions options = new OperaOptions();
                options.addArguments("--launch-maximized");
                options.addArguments("--lang=en-US");

                driverHelper.driver = new RemoteWebDriver(new URL(HUB_URL), options);
                driverHelper.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                break;
            }

            default: {
                LOG.error("Could not determine browser: " + browser + "\nAllow options are 'chrome', 'firefox'");
                break;
            }
        }
    }

    @After
    public void tearDown(Scenario scenario) {
        if (scenario.isFailed()) {
            LOG.error("Scenario failed");
            final byte[] screenshot = ((TakesScreenshot) driverHelper.driver).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot, String.valueOf(MediaType.PNG), scenario.getName());
        }

        if (driverHelper.driver != null) {
            LOG.debug("Closing WebDriver");
            driverHelper.driver.quit();
        }
    }
}
