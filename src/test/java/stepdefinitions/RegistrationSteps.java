package stepdefinitions;

import common.DriverHelper;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.HomePage;
import pages.RegistrationPage;
import pages.SignInPage;

import static pages.RegistrationPage.Gender;

public class RegistrationSteps {

    private final HomePage homePage;
    private final SignInPage signInPage;
    private final RegistrationPage registrationPage;

    public RegistrationSteps(DriverHelper driverHelper) {
        homePage = new HomePage(driverHelper.driver);
        signInPage = new SignInPage(driverHelper.driver);
        registrationPage = new RegistrationPage(driverHelper.driver);
    }

    @Given("I navigate to the registration page of the application")
    public void iNavigateToTheRegistrationPageOfTheApplication() {
        homePage.openHomePage();
        homePage.goToRegistrationPage();
    }

    @Then("The registration should be successful")
    public void theRegistrationShouldBeSuccessful() {
        registrationPage.verifyRegistrationSuccessful();
    }

    @Given("I enter my registration email")
    public void iEnterMyRegistrationEmail() {
        signInPage.enterCreationEmail();
    }

    @When("I click the create button")
    public void iClickTheCreateButton() {
        signInPage.clickCreateButton();
    }

    @Then("I should see the account creation form")
    public void iShouldSeeTheAccountCreationForm() {
        registrationPage.verifyAccountCreationFormDisplayed();
    }

    @Given("I enter my personal information")
    public void iEnterMyPersonalInformation() {
        registrationPage.enterPersonalInformation(Gender.FEMALE);
    }

    @When("I click the register button")
    public void iClickTheRegisterButton() {
        registrationPage.clickRegisterButton();
    }
}
