package stepdefinitions;

import common.DriverHelper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pages.HomePage;
import pages.SignInPage;

public class LoginSteps {

    private final HomePage homePage;
    private final SignInPage signInPage;

    public LoginSteps(DriverHelper driverHelper) {
        homePage = new HomePage(driverHelper.driver);
        signInPage = new SignInPage(driverHelper.driver);
    }

    @Given("I navigate to the login page of the application")
    public void iNavigateToTheLoginPageOfTheApplication() {
        homePage.openHomePage();
        homePage.goToSignInPage();
    }

    @And("I enter {string} as username and {string} as password")
    public void iEnterAsUsernameAndAsPassword(String username, String password) {
        signInPage.enterEmailAndPassword(username, password);
    }

    @And("I click login button")
    public void iClickLoginButton() {
        signInPage.clickSignInButton();
    }

    @Then("I should be logged in")
    public void iShouldBeLoggedIn() {
        homePage.verifyLoggedIn();
    }

    @Then("Login should be unsuccessful")
    public void loginShouldBeUnsuccessful() {
        signInPage.verifyErrorMessage();
    }
}
