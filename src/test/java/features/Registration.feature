Feature: RegistrationFeature
  This feature deals with the registration of a new user on the website

  Background:
    Given I navigate to the login page of the application

  Scenario: Successful registration
    Given I enter my registration email
    When I click the create button
    Then I should see the account creation form
    Given I enter my personal information
    When I click the register button
    Then The registration should be successful