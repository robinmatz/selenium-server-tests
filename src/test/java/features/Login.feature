Feature: LoginFeature
  This feature deals with the login functionality of the application

  Scenario: Login with correct username and password
    Given I navigate to the login page of the application
    And I enter "r.matz@test.com" as username and "8!BiF70Tlf55z$l" as password
    And I click login button
    Then I should be logged in

  Scenario: Login with incorrect username and password
    Given I navigate to the login page of the application
    And I enter "r.matz@test.com" as username and "falsePassword" as password
    And I click login button
    Then Login should be unsuccessful